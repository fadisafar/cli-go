package models

import (

	"time"
	"gorm.io/gorm"
)


type User struct {
    gorm.Model
    ID        uint   `gorm:"primaryKey"`
    Name      string `gorm:"not null"`
    Email     *string `gorm:"unique;not null" json:"email"`
    Password  string `gorm:"not null" json:"-"`
    Roles     []*Role `gorm:"many2many:user_roles;constraint:OnDelete:CASCADE;" json:"roles"`
    Groups    []*Group `gorm:"many2many:user_groups;constraint:OnDelete:CASCADE;" json:"groups"`
    CreatedAt time.Time `json:"created_at"`
    UpdatedAt time.Time `json:"updated_at"`
    DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}


type NewUser struct {
    FullName string    `json:"fullname"`
    Email    *string   `json:"email"`
    Password string    `json:"password"`
    RoleIDs  []uint  `json:"role_ids"`
    GroupIDs []uint  `json:"group_ids"`
}

type UserUpdate struct {
    Name        string   `json:"name"`
    Email       string   `json:"email"`
    Password    string   `json:"password"`
    RoleIDs  []uint  `json:"role_ids"`
    GroupIDs []uint  `json:"group_ids"`
}












