package models

import (
    "time"
    "gorm.io/gorm"
)


// swagger:model AuthToken
type AuthToken struct {
	gorm.Model
    // The ID of the authentication token.
    // required: true
    ID        uint   `gorm:"primaryKey"`
    // The authentication token.
    // required: true
    // swagger:strfmt uuid4
    Token     string `gorm:"unique;not null"`
    // The expiration date and time of the authentication token.
    // required: true
    ExpiresAt time.Time
    CreatedAt time.Time
    UpdatedAt time.Time
    // The deletion date and time of the user.
    // required: false
    DeletedAt time.Time `gorm:"index"`
    // The user ID associated with the authentication token.
    // required: true
    UserID     uint
    User       User
}
