package models

import (
	"time"
)


type Group struct {
	ID             uint           `gorm:"primaryKey"`
	Name           string         `gorm:"not null"`
	ParentGroupID  uint           `gorm:"index"`
	ParentGroup    *Group         `gorm:"foreignKey:ParentGroupID"`
	ChildGroups    []*Group       `gorm:"foreignKey:ParentGroupID;constraint:OnDelete:CASCADE;"`
	Users          []*User        `gorm:"many2many:user_groups;constraint:OnDelete:CASCADE;"`
    CreatedAt      time.Time
	UpdatedAt      time.Time
	DeletedAt      time.Time

}

type NewGroup struct {
    Name          string `json:"name" validate:"required"`
    ParentGroupID uint   `json:"parent_group_id"`
    ChildGroupsIDs []uint `json:"child_group_ids"`
}



type UpdatedGroup struct {
    Name           string
    ParentGroupID  uint
    ChildGroupsIDs []uint
}
