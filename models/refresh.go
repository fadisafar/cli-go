package models

import (
    "time"
)


type RefreshToken struct {
    // gorm.Model
    // The ID of the refresh token.
    // required: true
    ID         uint `gorm:"primaryKey"`
    // The token.
    // required: true
    // example: admin
    Token      string
    // The expiration date and time of the token.
    // required: false
    ExpiresAt  time.Time
    // The ID of the user.
    // required: true
    UserID     uint
    
    User       User
}