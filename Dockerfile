FROM golang:1.19

WORKDIR /app

COPY go.mod go.sum ./
RUN go mod download

COPY . .

RUN go build -o cli
# RUN CGO_ENABLED=0 GOOS=linux go build -o /docker-gs-ping

CMD ["./cli"]
