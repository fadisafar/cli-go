package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"CLI/cmd"
	"github.com/urfave/cli/v2"
)

type LoginResponse struct {
    AccessToken  string `json:"token"`
}

var LoginResponseData *LoginResponse

func main() {
    
	

    app := &cli.App{

        Name:  "CLI",
        Usage: "CLI tool to call an API",
        Commands: []*cli.Command{
			LoginCommand(),
            cmd.RolesCommand(),
			cmd.GroupsCommand(),
			cmd.UsersCommand(),
	
        },
    }

    err := app.Run(os.Args)
    if err != nil {
        cli.Exit(err.Error(), 1)
    }
}

func LoginCommand() *cli.Command {
    return &cli.Command{
        Name:  "login",
        Usage: "Login a user and receive a JWT authentication token and a refresh token",

        Flags: []cli.Flag{
            &cli.StringFlag{
                Name:     "email",
                Aliases:  []string{"e"},
                Usage:    "the user's email address",
                Required: true,
            },
            &cli.StringFlag{
                Name:     "password",
                Aliases:  []string{"p"},
                Usage:    "the user's password",
                Required: true,
            },
        },

        Action: func(c *cli.Context) error  {
            email := c.String("email")
            password := c.String("password")

            fmt.Println("Logging in...")

            // Check if email and password were provided
            if email == "" {
                return errors.New("email is required")
            }

            if password == "" {
                return errors.New("password is required")
            }

            // Make API call to login user and get JWT and refresh token
            apiUrl := "http://localhost:8080/auth/login"
            data := url.Values{}
            data.Set("email", email)
            data.Set("password", password)

            resp, err := http.PostForm(apiUrl, data)
            if err != nil {
                return err
            }
            defer resp.Body.Close()

			var loginResponse LoginResponse
			err = json.NewDecoder(resp.Body).Decode(&loginResponse)
			if err != nil {
				return err
			}

            LoginResponseData = &loginResponse

            fmt.Println(loginResponse.AccessToken)
            
            return nil
        },
    }
}
