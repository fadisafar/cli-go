package cmd

import (
	"CLI/models"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"

	"github.com/urfave/cli/v2"
)

// GroupsCommand returns a cli.Command for managing groups
func GroupsCommand() *cli.Command {
	return &cli.Command{
		Name:    "groups",
		Aliases: []string{"g"},
		Usage:   "manage groups",
		Subcommands: []*cli.Command{
			{
				Name:    "list-groups",
				Aliases: []string{"ls"},
				Usage:   "list all groups",
				Action:  ListGroups,
                Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:    "get-group",
				Aliases: []string{"gr"},
				Usage:   "get a specific group",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "id",
						Aliases:  []string{"i"},
						Usage:    "ID of the group to retrieve",
						Required: true,
					},
                    &cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
				Action: GetGroupById,
			},
			{
				Name:   "create-group",
				Usage:  "create a new group",
				Action: CreateGroup,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "name",
						Usage:    "group name",
						Required: true,
					},
					&cli.UintFlag{
						Name:     "parent-id",
						Usage:    "ID of the parent group",
						Required: false,
					},
					&cli.UintSliceFlag{
						Name:     "child-ids",
						Usage:    "IDs of the child groups",
						Required: false,
					},
                    &cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "update-group",
				Usage:  "update an existing group",
				Action: UpdateGroup,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "group-id",
						Aliases:  []string{"id"},
						Usage:    "group ID to update",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "name",
						Usage:    "new group name",
						Required: true,
					},
                    &cli.UintFlag{
						Name:     "parent-group-id",
						Usage:    "ID of the parent group",
						Required: false,
					},
					&cli.StringSliceFlag{
						Name:     "child-group-ids",
						Usage:    "IDs of the child groups",
						Required: false,
					},
                    &cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "delete-group",
				Usage:  "delete an existing group",
				Action: DeleteGroup,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "group-id",
						Aliases:  []string{"id"},
						Usage:    "group ID to delete",
						Required: true,
					},
                    &cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
		},
	}
}


func ListGroups(c *cli.Context) error {
    accessToken := c.String("access-token")
    url := "http://localhost:8080/groups"

    req, err := http.NewRequest("GET", url, nil)
    if err != nil {
        return err
    }

    req.Header.Set("Authorization", accessToken)

    req.Header.Set("Authorization", accessToken)

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    var groups []models.Group
    err = json.NewDecoder(resp.Body).Decode(&groups)
    if err != nil {
        return err
    }

    fmt.Printf("Groups:\n")
    for _, group := range groups {
        fmt.Printf("%d - %s\n", group.ID, group.Name)
    }

    return nil
}

func GetGroupByIdAction(id string, accessToken string) (*models.Group, error) {
    url := fmt.Sprintf("http://localhost:8080/groups/%s", id)

    req, err := http.NewRequest("GET", url, nil)
    if err != nil {
        return nil, err
    }

    req.Header.Set("Authorization", accessToken)

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
        return nil, err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        return nil, fmt.Errorf("unexpected status code: %d", resp.StatusCode)
    }

    var group models.Group
    if err := json.NewDecoder(resp.Body).Decode(&group); err != nil {
        return nil, err
    }

    if group.ID == 0 {
        return nil, fmt.Errorf("group not found with ID %s", id)
    }

    return &group, nil
}

func GetGroupById(c *cli.Context) error {
    var ErrNotFound = errors.New("record not found")
	accessToken := c.String("access-token")
	// id := c.Uint("id")
    id := c.String("id")

	group, err := GetGroupByIdAction(id, accessToken)
	if err != nil {
		if errors.Is(err, ErrNotFound) {
			return fmt.Errorf("group with ID %s not found", id)
		}
		return fmt.Errorf("failed to get group with ID %s: %s", id, err)
	}

	fmt.Printf("Group:\n- ID: %d, Name: %s\n", group.ID, group.Name)

	return nil
}

func CreateGroup(c *cli.Context) error {
    accessToken := c.String("access-token")
    Name := c.String("name")
    parentGroupID := c.Uint("parent-id")
    childGroupIDs := c.StringSlice("child-ids")

// create a new Group struct with the provided data
    group := models.Group{
        Name:          Name,
        ParentGroupID: parentGroupID,
    }

// if there are child group IDs, retrieve the child groups and add them to the new Group struct
    if len(childGroupIDs) > 0 {
        for _, childID := range childGroupIDs {
            childGroup, err := GetGroupByIdAction(childID, accessToken)
            if err != nil {
                return err
            }
            group.ChildGroups = append(group.ChildGroups, childGroup)
        }
    }

// send the Group struct to the external API
    err := SendGroupToAPI(&group, accessToken)
    if err != nil {
        return err
    }

    fmt.Printf("Group created: %s\n", group.Name)

    return nil
}

func SendGroupToAPI(group *models.Group, accessToken string) error {
    // Convert the group to a JSON byte slice
    jsonBytes, err := json.Marshal(group)
    if err != nil {
        return err
    }

    // Create a new HTTP request with the JSON data as the body
    req, err := http.NewRequest("POST", "http://localhost:8080/groups", bytes.NewBuffer(jsonBytes))
    if err != nil {
        return err
    }
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Authorization", accessToken)

    // Send the request
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }

    // Log the response body
    defer resp.Body.Close()
    respBody, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return err
    }
    fmt.Printf("Response body: %s\n", respBody)

    // Check the response status code
    if resp.StatusCode != http.StatusOK && resp.StatusCode != http.StatusCreated {
        return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
    }

    // Decode the response body into a Group struct
    var createdGroup models.Group
    if err := json.NewDecoder(bytes.NewReader(respBody)).Decode(&createdGroup); err != nil {
        return err
    }

    // Update the group passed as argument with the ID from the created group
    group.ID = createdGroup.ID

    return nil
}

func UpdateGroup(c *cli.Context) error {
	// Get the access token and group ID from the command line arguments
	accessToken := c.String("access-token")
	groupID := c.Uint("id")

	// Get the updated group fields from the command line arguments
	name := c.String("name")
	parentGroupIDString := c.String("parent-group-id")
	childGroupIDsString := c.StringSlice("child-group-ids")

	// Convert parentGroupIDString to uint
	var parentGroupID uint
	if parentGroupIDString != "" {
		parsed, err := strconv.ParseUint(parentGroupIDString, 10, 32)
		if err != nil {
			return err
		}
		parentGroupID = uint(parsed)
	}

	// Convert childGroupIDsString to []uint
	var childGroupIDs []uint
	for _, idString := range childGroupIDsString {
		parsed, err := strconv.ParseUint(idString, 10, 32)
		if err != nil {
			return err
		}
		childGroupIDs = append(childGroupIDs, uint(parsed))
	}

	// Construct the update payload
	payload := make(map[string]interface{})
	if name != "" {
		payload["name"] = name
	}
	if parentGroupID != 0 {
		payload["ParentGroupID"] = parentGroupID
	}
	if len(childGroupIDs) > 0 {
		// Use preload to include the child groups in the response
		payload["ChildGroups"] = map[string]interface{}{
			"IDs": childGroupIDs,
			"Preload": true,
		}
	}

	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	// Print the request body
	fmt.Println("Request body:")
	fmt.Println(string(jsonPayload))

	// Send the PUT request to the API
	req, err := http.NewRequest("PUT", fmt.Sprintf("http://localhost:8080/groups/%d", groupID), bytes.NewBuffer(jsonPayload))
	if err != nil {
		return err
	}
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", accessToken)

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Print the response body
	fmt.Println("Response body:")
	responseBody, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	fmt.Println(string(responseBody))

	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to update group: %s", resp.Status)
	}

	// Parse the updated group data from the response body
	var updatedGroup models.Group
	err = json.NewDecoder(bytes.NewReader(responseBody)).Decode(&updatedGroup)
	if err != nil {
		return err
	}

	fmt.Printf("Group %s updated successfully\n", updatedGroup.Name)
	fmt.Printf("Parent Group ID: %d\n", updatedGroup.ParentGroupID)
	fmt.Printf("Child Group IDs: %v\n", updatedGroup.ChildGroups)
	return nil
}


func DeleteGroup(c *cli.Context) error {
    accessToken := c.String("access-token")
    groupID := c.Uint("id")
    endpoint := fmt.Sprintf("http://localhost:8080/groups/%d", groupID)

    req, err := http.NewRequest(http.MethodDelete, endpoint, nil)
    if err != nil {
        return err
    }
    req.Header.Set("Authorization", accessToken)

    client := http.DefaultClient
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        return fmt.Errorf("unexpected status code: %d", resp.StatusCode)
    }

    fmt.Printf("Group with ID %d has been deleted\n", groupID)
    return nil
}



