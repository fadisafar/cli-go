package cmd

import (
	"CLI/models"
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"strconv"
	"net/http"
	"net/http/httputil"
	"github.com/urfave/cli/v2"
)

// UsersCommand returns a cli.Command for managing users
func UsersCommand() *cli.Command {
	return &cli.Command{
		Name:    "users",
		Aliases: []string{"u"},
		Usage:   "manage users",
		Subcommands: []*cli.Command{
			{
				Name:    "list-users",
				Aliases: []string{"ls"},
				Usage:   "list all users",
				Action:  ListUsers,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "get-user",
				Usage:  "retrieve a specific user",
				Action: GetUserByIDAction,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "user-id",
						Aliases:  []string{"id"},
						Usage:    "user ID to retrieve",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "create-user",
				Usage:  "create a new user",
				Action: CreateUser,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "name",
						Usage:    "user name",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "email",
						Usage:    "user email",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "password",
						Usage:    "user password",
						Required: true,
					},
					&cli.StringSliceFlag{
						Name:  "roles",
						Usage: "user roles",
					},
					&cli.StringSliceFlag{
						Name:  "groups",
						Usage: "user groups",
					},
				},
			},
			{
				Name:   "update-user",
				Usage:  "update an existing user",
				Action: UpdateUser,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "user-id",
						Aliases:  []string{"id"},
						Usage:    "user ID to update",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "name",
						Usage:    "new user name",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "email",
						Usage:    "new user email",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "password",
						Usage:    "new user password",
						Required: true,
					},
					&cli.StringSliceFlag{
						Name:  "roles",
						Usage: "new user roles",
					},
					&cli.StringSliceFlag{
						Name:  "groups",
						Usage: "new user groups",
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "delete-user",
				Usage:  "delete an existing user",
				Action: DeleteUser,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "user-id",
						Aliases:  []string{"id"},
						Usage:    "user ID to delete",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
		},
	}
}

func ListUsers(c *cli.Context) error {
    accessToken := c.String("access-token")
    req, err := http.NewRequest("GET", "http://localhost:8080/users", nil)
    if err != nil {
        return err
    }
    req.Header.Set("Authorization", accessToken)
    req.Header.Set("Accept", "application/json")

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusOK {
        return fmt.Errorf("error: %s", resp.Status)
    }

    var users []map[string]interface{}
    if err := json.NewDecoder(resp.Body).Decode(&users); err != nil {
        return err
    }

    for _, user := range users {
        userID := int(user["ID"].(float64))

        req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8080/users/%d", userID), nil)
        if err != nil {
            return err
        }
        req.Header.Set("Authorization", accessToken)
        req.Header.Set("Accept", "application/json")

        resp, err := client.Do(req)
        if err != nil {
            return err
        }
        defer resp.Body.Close()

        if resp.StatusCode != http.StatusOK {
            return fmt.Errorf("error: %s", resp.Status)
        }

        var userRolesGroups map[string]interface{}
        if err := json.NewDecoder(resp.Body).Decode(&userRolesGroups); err != nil {
            return err
        }

        groups := userRolesGroups["groups"].([]interface{})
        roles := userRolesGroups["roles"].([]interface{})

        fmt.Printf("User ID: %d, Name: %s, Email: %s\n", userID, user["Name"], user["Email"])
        fmt.Println("Groups:")
        for _, group := range groups {
            groupMap := group.(map[string]interface{})
            fmt.Printf("\tID: %d, Name: %s\n", int(groupMap["ID"].(float64)), groupMap["Name"])
        }
        fmt.Println("Roles:")
        for _, role := range roles {
            roleMap := role.(map[string]interface{})
            fmt.Printf("\tID: %d, Name: %s\n", int(roleMap["ID"].(float64)), roleMap["Name"])
        }
        fmt.Println("-----")
    }

    return nil
}


func GetUserByID(userID string, accessToken string) (string, error) {
    req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8080/users/%s", userID), nil)
    if err != nil {
        return "", err
    }
    req.Header.Set("Authorization", accessToken)

    resp, err := http.DefaultClient.Do(req)
    if err != nil {
        return "", err
    }
    defer resp.Body.Close()
	
    bodyBytes, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return "", err
    }
    bodyString := string(bodyBytes)
    fmt.Println("Response body:", bodyString)

    return bodyString, nil
}

func GetUserByIDAction(c *cli.Context) error {
    accessToken := c.String("access-token")
    userID := c.String("id")

    bodyString, err := GetUserByID(userID, accessToken)
    if err != nil {
        return err
    }

    var user map[string]interface{}
    if err := json.Unmarshal([]byte(bodyString), &user); err != nil {
        return err
    }

    userRolesGroups, ok := user["roles_groups"].(map[string]interface{})
    if !ok {
        return fmt.Errorf("invalid response body: missing roles_groups field")
    }

    var groups []interface{}
    if userRolesGroups["groups"] != nil {
        groups, _ = userRolesGroups["groups"].([]interface{})
    }

    var roles []interface{}
    if userRolesGroups["roles"] != nil {
        roles, _ = userRolesGroups["roles"].([]interface{})
    }

    fmt.Printf("User ID: %s, Name: %s, Email: %s\n", userID, user["Name"], user["Email"])
    fmt.Println("Groups:")
    for _, group := range groups {
        groupMap := group.(map[string]interface{})
        fmt.Printf("\tID: %d, Name: %s\n", int(groupMap["ID"].(float64)), groupMap["Name"])
    }
    fmt.Println("Roles:")
    for _, role := range roles {
        roleMap := role.(map[string]interface{})
        fmt.Printf("\tID: %d, Name: %s\n", int(roleMap["ID"].(float64)), roleMap["Name"])
    }
    fmt.Println("-----")

    return nil
}



func CreateUser(c *cli.Context) error {
    accessToken := c.String("access-token")
    fullname := c.String("name")
    email := c.String("email")
    password := c.String("password")
    roleIDs := c.StringSlice("roles")
    groupIDs := c.StringSlice("groups")

    // Validate inputs
    if fullname == "" || email == "" || password == "" {
        return fmt.Errorf("name, email, and password are required")
    }

    // Convert role IDs to uints
    var roleIDsUint []uint
    for _, roleIDStr := range roleIDs {
        roleID, err := strconv.ParseUint(roleIDStr, 10, 32)
        if err != nil {
            return err
        }
        roleIDsUint = append(roleIDsUint, uint(roleID))
    }

    // Convert group IDs to uints
    var groupIDsUint []uint
    for _, groupIDStr := range groupIDs {
        groupID, err := strconv.ParseUint(groupIDStr, 10, 32)
        if err != nil {
            return err
        }
        groupIDsUint = append(groupIDsUint, uint(groupID))
    }

    // Create user struct with input values
    user := &models.NewUser{
        FullName: fullname,
        Email:    &email,
        Password: password,
        RoleIDs:  roleIDsUint,
        GroupIDs: groupIDsUint,
    }

    // Convert user to JSON
    requestBody, err := json.Marshal(user)
    if err != nil {
        return err
    }

	fmt.Printf("Request body sent to endpoint: %s\n", string(requestBody)) // Print the request body
    // Create HTTP client
    client := &http.Client{}

    // Set up HTTP request
    url := "http://localhost:8080/users"
    req, err := http.NewRequest("POST", url, bytes.NewBuffer(requestBody))
    if err != nil {
        return err
    }

    // Set headers
    req.Header.Set("Content-Type", "application/json")
    req.Header.Set("Authorization", accessToken)

    fmt.Printf("Sending request to %s with access token: %s\n", url, accessToken)

    // Send request
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    // Read response body
    body, err := ioutil.ReadAll(resp.Body)
    if err != nil {
        return err
    }
    fmt.Printf("Response received from %s:\n", url)
    fmt.Println(string(body))

    // Unmarshal response body into user struct
    err = json.Unmarshal(body, &user)
    if err != nil {
        return err
    }

    fmt.Printf("User %s created successfully\n", user.FullName)
    return nil
}

func UpdateUser(c *cli.Context) error {
	// Get the access token, user ID, name, email, password, role IDs, and group IDs from the command line arguments
	accessToken := c.String("access-token")
	userID := c.Uint("user-id")
	name := c.String("name")
	email := c.String("email")
	password := c.String("password")
	roleIDs := c.StringSlice("roles")
	groupIDs := c.StringSlice("groups")

	// Create a map to store the payload data
	payload := make(map[string]interface{})

	// If the name is not empty, add it to the payload
	if name != "" {
		payload["name"] = name
	}

	// If the email is not empty, add it to the payload
	if email != "" {
		payload["email"] = email
	}

	// If the password is not empty, add it to the payload
	if password != "" {
		payload["password"] = password
	}

	// If there are role IDs specified, convert them to uints and add them to the payload
	if len(roleIDs) > 0 {
		roleIDsUint := make([]uint, len(roleIDs))
		for i, roleIDStr := range roleIDs {
			roleID, err := strconv.ParseUint(roleIDStr, 10, 32)
			if err != nil {
				return err
			}
			roleIDsUint[i] = uint(roleID)
		}
		payload["role_ids"] = roleIDsUint
	}

	// If there are group IDs specified, convert them to uints and add them to the payload
	if len(groupIDs) > 0 {
		groupIDsUint := make([]uint, len(groupIDs))
		for i, groupIDStr := range groupIDs {
			groupID, err := strconv.ParseUint(groupIDStr, 10, 32)
			if err != nil {
				return err
			}
			groupIDsUint[i] = uint(groupID)
		}
		payload["group_ids"] = groupIDsUint
	}

	// Convert the payload to JSON
	jsonPayload, err := json.Marshal(payload)
	if err != nil {
		return err
	}

	// Create a new PUT request to update the user with the specified ID
	req, err := http.NewRequest("PUT", fmt.Sprintf("http://localhost:8080/users/%d", userID), bytes.NewBuffer(jsonPayload))
	if err != nil {
		return err
	}

	// Set the request headers
	req.Header.Set("Content-Type", "application/json")
	req.Header.Set("Authorization", accessToken)

	// Send the request to the server and get the response
	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	// Check the response status code
	if resp.StatusCode != http.StatusOK {
		return fmt.Errorf("failed to update user: %s", resp.Status)
	}

	// Read the response body
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	
	// Print the response body
	fmt.Printf("Response body: %s\n", string(body))

	// Decode the response body into a UserUpdate struct
	var updatedUser models.UserUpdate
	err = json.NewDecoder(resp.Body).Decode(&updatedUser)
	if err != nil {
		return err
	}

	// Dump the request information for debugging purposes
	dump, err := httputil.DumpRequest(req, true)
	if err != nil {
		return err
	}
	fmt.Printf("Request sent to %s:\n%s\n", req.URL.String(), string(dump))
	fmt.Printf("Request body: %s\n", string(jsonPayload))

	// Print a

    fmt.Printf("User %s updated successfully\n", updatedUser.Name)
    return nil
}



func DeleteUser(c *cli.Context) error {
    accessToken := c.String("access-token")
    userID := c.String("id")
    if userID == "" {
        return fmt.Errorf("userID is required")
    }

    // Send DELETE request to external API
    client := &http.Client{}
    url := fmt.Sprintf("http://localhost:8080/users/%s", userID)
    req, err := http.NewRequest("DELETE", url, nil)
    if err != nil {
        return err
    }
    req.Header.Set("Authorization", accessToken)

    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusNoContent {
        return fmt.Errorf("failed to delete user with ID %s", userID)
    }

    fmt.Printf("User with ID deleted successfully\n")
    return nil
}



