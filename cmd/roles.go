package cmd

import (
	"CLI/models"
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"github.com/urfave/cli/v2"
)



func RolesCommand() *cli.Command {
	return &cli.Command{
		Name:    "roles",
		Aliases: []string{"r"},
		Usage:   "manage roles",
		Subcommands: []*cli.Command{
			{
				Name:    "list-roles",
				Aliases: []string{"lr"},
				Usage:   "list all roles",
				Action: ListRoles,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:    "get-role",
				Aliases: []string{"gr"},
				Usage:   "get a specific role",
				Action: GetRoleByIDAction,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "id",
						Aliases:  []string{"i"},
						Usage:    "ID of the role to retrieve",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:  "create-role",
				Usage: "create a new role",
				Action: CreateRole,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "name",
						Usage:    "new role name",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "description",
						Usage:    "new role description",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "update-role",
				Usage:  "update an existing role",
				Action: UpdateRole,
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "role-id",
						Aliases:  []string{"id"},
						Usage:    "role ID to update",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "name",
						Usage:    "new role name",
						Required: true,
					},
					&cli.StringFlag{
						Name:     "description",
						Usage:    "new role description",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
			{
				Name:   "delete-role",
				Usage:  "delete an existing role",
				Action: DeleteRole, // assign the function to the Action field
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:     "role-id",
						Aliases:  []string{"id"},
						Usage:    "role ID to delete",
						Required: true,
					},
					&cli.StringFlag{
						Name:  "access-token",
						Usage: "Access Token",
						Required: true,
					},
				},
			},
		},
	}
}
			

func ListRoles(c *cli.Context) error {
    accessToken := c.String("access-token")
    url := "http://localhost:8080/roles"
    req, err := http.NewRequest("GET", url, nil)
    if err != nil {
        return err
    }

    req.Header.Set("Authorization", accessToken)

    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }

    var roles []models.Role
    err = json.NewDecoder(resp.Body).Decode(&roles)
    if err != nil {
        return err
    }

    for _, role := range roles {
        fmt.Printf("%d - %s\n", role.ID, role.Name)
    }

    fmt.Printf("Response Status: %s\n", resp.Status)
    // fmt.Printf("Response Headers: %v\n", resp.Header)

    return nil
}

func GetRoleByID(roleID string, accessToken string) (*models.Role, error) {
	req, err := http.NewRequest("GET", fmt.Sprintf("http://localhost:8080/roles/%s", roleID), nil)
	if err != nil {
		return nil, err
	}
	req.Header.Set("Authorization", accessToken)

	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var role models.Role
	err = json.Unmarshal(body, &role)
	if err != nil {
		return nil, err
	}

	return &role, nil
}

func GetRoleByIDAction(c *cli.Context) error {
	accessToken := c.String("access-token")
    roleID := c.String("id")
    role, err := GetRoleByID(roleID, accessToken)
    if err != nil {
        return err
    }
    fmt.Printf("%d - %s\n", role.ID, role.Name)
    return nil
}

func CreateRole(c *cli.Context) error {
	accessToken := c.String("access-token")
	name := c.String("name")
	description := c.String("description")

    // Create a new HTTP request with the access token in the header
    req, err := http.NewRequest("POST", "http://localhost:8080/roles", nil)
    if err != nil {
        return err
    }
    req.Header.Set("Authorization", accessToken)

    // Encode the role data in JSON format and set the request body
    roleData := map[string]interface{}{"name": name, "description": description}
    body, err := json.Marshal(roleData)
    if err != nil {
        return err
    }
    req.Body = ioutil.NopCloser(bytes.NewReader(body))
    req.Header.Set("Content-Type", "application/json")

    // Send the request and check for errors
    resp, err := http.DefaultClient.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusCreated {
        return fmt.Errorf("Failed to create role. Status code: %d", resp.StatusCode)
    }

    fmt.Printf("Role %s created successfully\n", name)

    return nil
}


func UpdateRole(c *cli.Context) error {
	id := c.String("id")
	name := c.String("name")
	description := c.String("description")
	accessToken := c.String("access-token")

	// Create the JSON request body
	requestBody := map[string]interface{}{
		"name":        name,
		"description": description,
	}

	// Marshal the request body to JSON
	jsonBody, err := json.Marshal(requestBody)
	if err != nil {
		return err
	}

	// Create the HTTP client and request
	client := &http.Client{}
	url := fmt.Sprintf("http://localhost:8080/roles/%s", id)
	req, err := http.NewRequest("PUT", url, bytes.NewBuffer(jsonBody))
	if err != nil {
		return err
	}

	// Set the request header
	req.Header.Set("Authorization", accessToken)
	req.Header.Set("Content-Type", "application/json")

	// Send the request and get the response
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

    // Check the status code of the HTTP response
    if resp.StatusCode == http.StatusNotFound {
        return fmt.Errorf("role with ID %s not found", id)
    } else if resp.StatusCode != http.StatusOK {
        return fmt.Errorf("failed to update role with ID %s", id)
    }

	fmt.Printf("Role %s updated successfully\n", name)

	return nil
}



func DeleteRole(c *cli.Context) error {
    roleId := c.Int("role-id")
    accessToken := c.String("access-token")
    if roleId == 0 {
        return errors.New("missing required flag --role-id")
    }

    // Create a new HTTP request with the access token in the header
    req, err := http.NewRequest("DELETE", fmt.Sprintf("http://localhost:8080/roles/%d", roleId), nil)
    if err != nil {
        return err
    }
    req.Header.Set("Authorization", accessToken)

    // Send the HTTP request
    client := &http.Client{}
    resp, err := client.Do(req)
    if err != nil {
        return err
    }
    defer resp.Body.Close()

    if resp.StatusCode != http.StatusNoContent {
        // Read the error response body and return it as an error
        errMsg, err := ioutil.ReadAll(resp.Body)
        if err != nil {
            return fmt.Errorf("failed to delete role with ID %d: %s", roleId, resp.Status)
        }
        err = fmt.Errorf("failed to delete role with ID %d: %s", roleId, string(errMsg))
        fmt.Println(err)
        return err
    }

    fmt.Printf("Role with ID deleted successfully\n")

    return nil
}


