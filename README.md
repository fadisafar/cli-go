API CLI
This is a CLI tool for interacting with the API. It provides a set of commands for managing users, roles, and user groups.

Installation
To install the CLI, you can use Docker and Docker Compose. First, clone the repository:

git clone https://gitlab.com/fadisafar/cli.git

Then, build the Docker image:
docker-compose build
Finally, run the CLI



Commands
Login
The login command allows the admin to log in to the API and receive a JWT authentication token. Just the admin is capable to do operation on database.

go run main.go login --email <email> --password <password>

Admin is the only user authorized to login and do all operations. Admins login and
gets an access token that authorize him to GET, POST, PUT and DELETE.
Admin Information:
email: admin@gmail.com
password: admin1234

go run main.go login --email admin@gmail.com --password admin1234

Options:
--email: the user's email address
--password: the user's password

//TODO//
Refresh
The refresh command allows you to renew a JWT authentication token using a refresh token.
api-cli refresh --refresh-token <refresh-token>
Options:
--refresh-token: the refresh token

Logout
The logout command allows you to remove a JWT and refresh authentication token.
api-cli logout --access-token <access-token> --refresh-token <refresh-token>
Options:
--access-token: the authentication token to remove
--refresh-token: the refresh token to delete

/////////////////////


Users
The users command allows you to manage users.

List
The list subcommand lists all users.
go run main.go users list-users --access-token <access-token>

Get
The get subcommand retrieves a specific user.
go run main.go users get-user --id <user-id> --access-token <access-token>

Options:
-id: the user ID to retrieve

Create
The create subcommand creates a new user.
go run main.go users create-user --access-token <access-token> --name=<name> --email=<email> --password= <password> --roles=<rolesID> --groups=<groupsID> 

Options:
--name: the name of the user
--email: the user's email address
--password: the user's password
--roles: the user's roles (potentially empty)
--groups: the user's groups (potentially empty)
--access-token: the access-token given to admin when logged in 

Update
The update subcommand updates an existing user.
go run main.go users update-user --id <user-id> --name <name> --email <email> --password <password> 
--roles <rolesID/rolesIDs> --groups <groupsID>  --access-token <access-token>

Options:
--id: the user ID to update
--name: the new user name
--email: the user's new email address
--password: the user's new password
--roles: new user roles ID/IDs
--groups: the user's new groups ID
--access-token: the access-token given to admin when logged in 

//TODO
Unapble to update user's role ids and users's group ids

Delete
The delete subcommand deletes an existing user.
go run main.go users delete-user --id <user-id> --access-token <access-token>

Options:
--id: the user ID to delete
--access-token: the access-token given to admin when logged in 


Roles
The CLI allows managing roles through the following commands:

List roles
List all existing roles:
go run main.go roles list-roles --access-token <access-token>

Options:
--access-token: the access-token given to admin when logged in 


Get role
Retrieve a specific role using its ID:
go run main.go roles get-role --id <role_id> --access-token <access-token>

Options:
--access-token: the access-token given to admin when logged in 
--id: the Role ID to get

Create role
Create a new role with a name and description:
go run main.go roles create-role --name <role_name> --description <role_description> --access-token <access-token>


Options:
--name : The name of the new role
--description: description of the new role
--access-token: the access-token given to admin when logged in 


Update role
Update an existing role using its ID, and provide a new name and description:
go run main.go roles update-role --id <role_id> --name <new_role_name> --description <new_role_description>
--access-token <access-token>

Options:
--name : the new name of the updated role
--description: new description of the updated role
--access-token: the access-token given to admin when logged in 

Delete role
Delete an existing role using its ID:
go run main.go roles delete-role --id <role_id> --access-token <access_token>

Options:
--access-token: the access-token given to admin when logged in 
--id: the Role ID to delete

When role is assigned to users the delete-role command line returns an error {"error":"ERROR: update or delete on table \"roles\" violates foreign key constraint \"fk_user_roles_role\" on table \"user_roles\" (SQLSTATE 23503)"}


Groups
The CLI allows managing user groups using the following commands:

List groups
To list all groups, use the following command:
go run main.go groups list-groups --access-token <access_token>

Options:
--access-token: the access-token given to admin when logged in 


Get a specific group
To retrieve a specific group by its ID, use the following command:

go run main.go groups get-group --id <group_id> --access-token <access_token>
Replace <group_id> with the ID of the group to retrieve.

Options:
--access-token: the access-token given to admin when logged in 
--id: the ID of group to get

Create a new group
To create a new group, use the following command:
go run main.go groups create-group --name <name> --parent-id <parent_id> --child-ids <child_ids>  --access-token <access-token>

Options:
--access-token: the access-token given to admin when logged in 
--name: the name of the new group
--parent-group-id: the ID of the parent group
--child-ids: the ID/IDs of its child groups.

Update a group
To update an existing group, use the following command:
go run main.go groups update-group --id <group_id> --name <new_name> --parent-group-id <parent_group-id> --access-token <access-token>

Options:
--access-token: the access-token given to admin when logged in 
--id: the ID of the group to update
--name: the new name of the group
--parent-group-id: the new parent group id


Delete a group
To delete an existing group, use the following command:
go run main.go groups delete-group --group-id <group_id> --access-token <access-token>

Options:
--access-token: the access-token given to admin when logged in 
--id: the ID of the group to delete